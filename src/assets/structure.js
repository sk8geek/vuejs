
export default {
 data () {
   topics: [
     {
       id: 1,
       target: 'topic',
       title: 'Sports Centre',
       slides: [
         {
           id: 1,
           title: 'Sports Centre 1',
           background: 'sports1.jpg',
           timer: '4000',
           audio: 'sports_audio1.mp3'
         },
         {
           id: 2,
           title: 'Sports Centre 2',
           background: 'sports2.jpg',
           timer: '0',
           audio: 'sports_audio2.mp3'
         }
       ]
     },
     { id: 2, target: 'topic', title: 'Shopping Mall' }
   ]
 }
}
