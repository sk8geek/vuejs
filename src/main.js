// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import App from './App'
import Vue from 'vue'
import VueRouter from 'vue-router'
import Contents from '@/components/Contents'
import Topic from '@/components/Topic'
// import Slide from '@/components/Slide'

Vue.use(VueRouter)
Vue.config.productionTip = false

const routes = [
  { path: '/', component: Contents, props: true },
  { path: 'topic/:id', component: Topic, props: true }
]

const router = new VueRouter({
  routes
})

/* eslint-disable no-new */
new Vue({
  router,
  el: '#app',
  template: '<App/>',
  components: { App }
}).$mount('#app')
